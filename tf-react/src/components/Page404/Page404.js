import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Page404.css';

class Page404 extends Component {
	render(){
		return(
			<div className="page-404 container">
			<img src={this.props.img} alt="404"/>
			<h1>404</h1>
			<p>Page not found</p>
			<Link to="/"><button>Go back home</button></Link>
			</div>
			)
	}
}

export default Page404;