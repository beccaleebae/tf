import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
	render(){
		const links = this.props.links.map((l, index) => {
			return (
				<a href={l.link} key={index} target="_blank" rel="noopener noreferrer"><img src={l.icon} alt=""/></a>)
		})
		return (
			<footer style={{display: this.props.responseOk ? "flex": "none"}}>
				<p>&copy; Toreador Films LLC. All rights reserved.</p>
				<div>
				{links}
				</div>
			</footer>
			)
	}
}

export default Footer;