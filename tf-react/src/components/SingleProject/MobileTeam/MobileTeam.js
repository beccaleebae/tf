import React, { Component } from 'react';
import './MobileTeam.css';

class MobileTeam extends Component {
	render(){
		console.log(this.props.data);
		const renderMobileTeam = this.props.data.map((m, index) => {
			return (
				<div className="mobile-member-section" key={index}>
					<p className="mob-title">{m.title}</p>
					<p className="name">{m.name}</p>
				</div>
				)
		})
		return(
			<div className="mobile-team-section">
			<p className="title">TEAM</p>
			<div className="mobile-members">
			{renderMobileTeam}
			</div>
			</div>)
	}
}

export default MobileTeam;