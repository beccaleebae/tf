import React, { Component } from 'react';
import './Laurels.css';

class Laurels extends Component {
	render(){
		let renderLaurels = this.props.data.map((l, index) => {
			return(<img src={l} key={index} alt="Laurel"/>)
		})
		return(<div className="laurels-row">{renderLaurels}</div>)
	}
}

export default Laurels;