import React, { Component } from "react";
import Loading from "../Loading/Loading";
import HeroVideo from ".././HeroVideo/HeroVideo";
import TeamLeftNav from "./TeamLeftNav/TeamLeftNav";
import MobileTeam from "./MobileTeam/MobileTeam";
import ProjText from "./ProjText/ProjText";
import ProdPhotos from "./ProdPhotos/ProdPhotos";
import Laurels from "./Laurels/Laurels";
import "./SingleProject.css";

class SingleProject extends Component {
	componentWillMount() {
		window.scrollTo(0, 0);
		this.props.changeRoute();
		this.props.getPageData(
			`project/${this.props.match.params.id}`,
			"projectData"
		);
	}
	render() {
		if (this.props.responseOk === true) {
			const data = this.props.data;
			return (
				<div className="container">
					{data.video && <HeroVideo src={data.video} />}
					<div className="single-proj-row">
						<div className="single-proj-left">
							{data.team && <TeamLeftNav data={data.team} />}
						</div>
						{data && (
							<div className="single-proj-right">
								<ProjText data={data} />
								{data.fullLength && (
									<a href={data.fullLength} target="_blank" rel="noopener noreferrer">
										<button className="full-film-button">Watch Movie</button>
									</a>
								)}
								{data.team && <MobileTeam data={data.team} />}
								{data.productionStills && (
									<ProdPhotos data={data.productionStills} />
								)}
								{data.nominations && <Laurels data={data.nominations} />}
							</div>
						)}
					</div>
				</div>
			);
		} else {
			return <Loading />;
		}
	}
}

export default SingleProject;