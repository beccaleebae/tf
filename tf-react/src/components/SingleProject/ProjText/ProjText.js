import React, { Component } from "react";
import "./ProjText.css";

class ProjText extends Component {
	render() {
		let data = this.props.data;
		return (
			<div>
				{data.client && (
					<section>
						<p className="proj-section-header">Client</p>
						<p>{data.client}</p>
					</section>
				)}

				<section>
					<p className="proj-section-header">{data.title}</p>
					<p>{data.summary}</p>
				</section>
			</div>
		);
	}
}

export default ProjText;