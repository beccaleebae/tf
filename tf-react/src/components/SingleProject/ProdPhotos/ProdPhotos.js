import React, { Component } from 'react';
import './ProdPhotos.css';

class ProdPhotos extends Component {
	render(){
		let renderProdPhotos = this.props.data.map((p, index) => {
			return (<img src={p} key={index} alt="Production Still"/>)
		})
		return(<div className="prod-stills">
			<p className="proj-section-header">Production Stills</p>
			{renderProdPhotos}
			</div>)
	}
}

export default ProdPhotos;