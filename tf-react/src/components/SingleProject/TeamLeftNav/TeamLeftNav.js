import React, { Component } from 'react';
import './TeamLeftNav.css';

class TeamLeftNav extends Component {
	render(){
		let renderTeam = this.props.data.map((m, index) => {
			return (
				<div className="team-member" key={index}>
					<p className="title">{m.title}</p>
					<p className="name">{m.name}</p>
				</div>)
		})
		return(<div className="team-left-nav">{renderTeam}</div>)
	}
}

export default TeamLeftNav;