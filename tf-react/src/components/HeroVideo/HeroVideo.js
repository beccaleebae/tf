import React, { Component } from "react";
import "./HeroVideo.css";

class HeroVideo extends Component {
	render() {
		return (
			<div className="iframe-container">
			<iframe title="Reel Video" src={this.props.src} allowFullScreen/>
			</div>
		);
	}
}

export default HeroVideo;