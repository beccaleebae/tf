import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './ProjTile.css';

class ProjTile extends Component {
	render(){
		let d = this.props.data;
		return(
			<Link to={`/work/${d.id}`} className="link-container">
			<div className="proj-tile">
			<img src={d.thumbnail} alt={d.thumbnail}/>
			</div>
			</Link>
			)
	}
}

export default ProjTile;