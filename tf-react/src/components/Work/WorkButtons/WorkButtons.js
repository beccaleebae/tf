import React, { Component } from "react";
import "./WorkButtons.css";

class WorkButtons extends Component {
	render() {
		return (
			<div className="work-controller">
				<button
					onClick={() => this.props.toggleWorkGrid()}
					className={this.props.viewCommercialWork ? "w-inactive" : "w-active"}
				>
					Narrative
				</button>
				<button
					onClick={() => this.props.toggleWorkGrid()}
					className={this.props.viewCommercialWork ? "w-active" : "w-inactive"}
				>
					Commercial
				</button>
			</div>
		);
	}
}

export default WorkButtons;