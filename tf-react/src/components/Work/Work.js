import React, { Component } from "react";
import Loading from "../Loading/Loading";
import HeroVideo from "../HeroVideo/HeroVideo";
import WorkButtons from "./WorkButtons/WorkButtons";
import ProjGrid from "./ProjGrid/ProjGrid";
import "./Work.css";

class Work extends Component {
	componentWillMount() {
		this.props.changeRoute();
		this.props.getPageData("work", "workPageData");
	}
	render() {
		if (this.props.responseOk === true) {
			let w = this.props.workPageData;
			return (
				<div className="container">
					{w.reelVideo && <HeroVideo src={w.reelVideo} />}
					<WorkButtons
						toggleWorkGrid={this.props.toggleWorkGrid}
						viewCommercialWork={this.props.viewCommercialWork}
					/>
					{w.projects && (
						<ProjGrid
							viewCommercialWork={this.props.viewCommercialWork}
							projectsData={w.projects}
						/>
					)}
				</div>
			);
		} else {
			return <Loading />;
		}
	}
}

export default Work;