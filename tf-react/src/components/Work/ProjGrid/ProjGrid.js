import React, { Component } from "react";
import ProjTile from "../ProjTile/ProjTile";
import "./ProjGrid.css";

class ProjGrid extends Component {
	render() {
		let view;

		if (this.props.viewCommercialWork === true) {
			view = "commercial";
		} else {
			view = "narrative";
		}

		let gridData = this.props.projectsData.filter(w => {
			return w.projectType === view;
		});

		let renderGrid = gridData.map((t, index) => {
			return t.thumbnail !== null && (<ProjTile key={index} data={t} />)
		});
		return <div className="proj-grid">{renderGrid}</div>;
	}
}

export default ProjGrid;