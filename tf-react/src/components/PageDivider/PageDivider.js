import React, { Component } from "react";
import "./PageDivider.css";

class PageDivider extends Component {
	render() {
		return (
			<div
				className={this.props.miniDivider ? "page-divider divider-sm" : "page-divider divider-lg"}
				style={{ margin: this.props.marginLg ? "40px 0" : "0px" }}
			/>
		);
	}
}

export default PageDivider;