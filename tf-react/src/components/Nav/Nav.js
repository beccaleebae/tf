import React, { Component } from "react";
import { Link } from 'react-router-dom';
import NavLogo from "./NavLogo/NavLogo";
import "./Nav.css";

class Nav extends Component {
	render() {
		const tabs = this.props.tabs.map((tab, index) => {
			let t = tab.charAt(0).toUpperCase() + tab.slice(1);
			return (
				<Link key={index} to={`/${tab}`}>
					{t}
				</Link>
			);
		});
		return (
			<nav className={this.props.showNavShadow ? "nav-on-scroll" : ""}>
				<NavLogo logo={this.props.logo} />
				<div>{tabs}</div>
			</nav>
		);
	}
}

export default Nav;