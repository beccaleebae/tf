import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./NavLogo.css";

class NavLogo extends Component {
	render() {
		return (
			<Link to="/">
			<div className="nav-logo-content">
				<img src={this.props.logo} alt="TF Logo" />
				<p>TOREADOR FILMS</p>
			</div>
			</Link>
		);
	}
}

export default NavLogo;