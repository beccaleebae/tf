import React, { Component } from 'react';
import Poster from '../../Poster/Poster';
import './PostersRow.css';

class PostersRow extends Component {
	render(){
		const posters = this.props.d.posters.map((p, index) => {
				return <Poster key={index} img={p} />;
			});
		return(
			<div className="poster-row">{posters}</div>
			)
	}
}

export default PostersRow;