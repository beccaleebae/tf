import React, { Component } from "react";
import "./IntroText.css";

class IntroText extends Component {
	render() {
		return (
			<p className="home-intro-text">
				{this.props.text}
			</p>
		);
	}
}

export default IntroText;