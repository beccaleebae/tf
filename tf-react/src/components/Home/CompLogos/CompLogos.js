import React, { Component } from 'react';
import './CompLogos.css';

class CompLogos extends Component {
	render(){
		const l = this.props.logos.map((logo, index) => {
			return (
				<img key={index} src={logo} alt="Company Logo"/>)
		})
		return(
			<div className="comp-logo-row">{l}</div>
			)
	}
}

export default CompLogos;