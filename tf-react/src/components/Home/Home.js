import React, { Component } from "react";
import Loading from "../Loading/Loading";
import PageDivider from "../PageDivider/PageDivider";
import IntroText from "./IntroText/IntroText";
import PostersRow from "./PostersRow/PostersRow";
import CompLogos from "./CompLogos/CompLogos";
import "./Home.css";

class Home extends Component {
	componentWillMount() {
		this.props.changeRoute();
		this.props.getPageData("home", "homePageData");
	}
	render() {
		if (this.props.responseOk === true) {
			const d = this.props.data;
			return (
				<div className="home-container container">
					<PageDivider marginLg={true} />
					{d.introText && <IntroText text={d.introText} />}
					{d.subText && <p className="hp-sub-text">{d.subText}</p>}
					{d.posters && <PostersRow d={d} />}
					<PageDivider marginLg={true} />
					{d.logos && <CompLogos logos={d.logos} />}
				</div>
			);
		} else {
			return <Loading />;
		}
	}
}

export default Home;