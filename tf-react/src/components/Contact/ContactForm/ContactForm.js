import React, { Component } from "react";
import "./ContactForm.css";

class ContactForm extends Component {
	render() {
		return (
			<div className="contact-half">
				<form onSubmit={e => this.props.onSubmit(e)}>
					<div className="form-row">
						<label htmlFor="first-name">First Name</label>
						<input
							type="text"
							onChange={e => this.props.onChange(e, 'firstName')}
							value={this.props.emailFormData.firstName}
							required
						/>
					</div>
					<div className="form-row">
						<label htmlFor="last-name">Last Name</label>
						<input
							type="text"
							onChange={e => this.props.onChange(e, 'lastName')}
							value={this.props.emailFormData.lastName}
							required
						/>
					</div>
					<div className="form-row">
						<label htmlFor="email">Email</label>
						<input
							type="email"
							onChange={e => this.props.onChange(e, 'email')}
							value={this.props.emailFormData.email}
							required
						/>
					</div>
					<div className="form-row">
						<label htmlFor="subject">Subject</label>
						<input
							type="text"
							onChange={e => this.props.onChange(e, 'subject')}
							value={this.props.emailFormData.subject}
							required
						/>
					</div>
					<div className="form-row">
						<label htmlFor="body">Message</label>
						<textarea
							rows="10"
							onChange={e => this.props.onChange(e, 'body')}
							value={this.props.emailFormData.body}
							required
						/>
					</div>
					<div className="form-row">
						<input type="submit" value="SEND" />
					</div>
				</form>
			</div>
		);
	}
}

export default ContactForm;