import React, { Component } from "react";
import Loading from '../Loading/Loading';
import ContactText from "./ContactText/ContactText";
import ContactForm from "./ContactForm/ContactForm";
import "./Contact.css";

class Contact extends Component {
	componentDidMount(){
		this.props.changeRoute();
		this.props.getPageData('contact', 'contactData');
	}
	render() {
		if(this.props.responseOk === true) {
return (
			<div className="contact-page container">
				<ContactText data={this.props.contactData} />
				<ContactForm
					data={this.props.contactData}
					emailFormData={this.props.emailFormData}
					onSubmit={this.props.onSubmit}
					onChange={this.props.onChange}
				/>
			</div>
		);
		} else {
			return <Loading/>;
		}
	}
}

export default Contact;