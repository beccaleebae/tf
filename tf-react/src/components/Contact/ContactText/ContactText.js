import React, { Component } from "react";
import PageDivider from '../../PageDivider/PageDivider';
import './ContactText.css';

class ContactText extends Component {
	render(){
		let d = this.props.data;
		return(
			<div className="contact-half">
			<PageDivider miniDivider={true}/>
			<p className="contact-header">Contact Us</p>
			<p>{d.text}</p>
			<p className="contact-address">{d.address1}</p>
			<p className="contact-address">{d.address2}</p>
			<a href={`mailto:${d.email}`}>{d.email}</a>
			</div>
			)
	}
}

export default ContactText;