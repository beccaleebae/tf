import React, { Component } from "react";
import Member from "./Member/Member.js";
import Loading from "../Loading/Loading";
import "./Team.css";

class Team extends Component {
	componentDidMount() {
		this.props.changeRoute();
		this.props.getPageData("team", "teamPageData");
	}
	render() {
		if (this.props.responseOk === true) {
			let renderTeamMembers = this.props.teamPageData.map((m, index) => {
				return <Member key={index} data={m} />;
			});
			return <div className="team-page container">{renderTeamMembers}</div>;
		} else {
			return <Loading />;
		}
	}
}

export default Team;