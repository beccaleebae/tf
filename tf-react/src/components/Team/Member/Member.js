import React, { Component } from 'react';
import PageDivider from '../../PageDivider/PageDivider';
import './Member.css';

class Member extends Component {
	render(){
		let d = this.props.data;
		return(
			<div className="member-content">
			<img src={d.img} alt="Member Headshot"/>
			<p className="member-name">{d.name}</p>
			<p className="member-email">{d.email}</p>
			<PageDivider miniDivider={true}/>
			<p className="member-bio">{d.bio}</p>
			</div>
			)
	}
}

export default Member;