import React, { Component } from 'react';
import './Poster.css';

class Poster extends Component {
	render(){
		return(
			<img src={this.props.img} alt="Movie Poster" className="poster-img"/>
			)
	}
}

export default Poster;