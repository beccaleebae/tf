import React, { Component } from "react";
import Logo from "../../assets/ToreadorFilmsLogo.png";
import './Loading.css';

class Loading extends Component {
   componentWillUnmount() {
      console.log('Loading component unmounting')
   }
	render(){
		return(
			<div className="loading-screen container">
			<img src={Logo} alt="Loading Page"/>
			</div>
			)
	}
}

export default Loading;