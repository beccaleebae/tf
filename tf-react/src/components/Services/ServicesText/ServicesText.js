import React, { Component } from 'react';
import './ServicesText.css';

class ServicesText extends Component {
	render(){
		return(
			<div className="services-text">
			<p className="services-main-copy">{this.props.mainCopy}</p>
			<p className="services-sub-copy">{this.props.subCopy}</p>
			</div>
			)
	}
}

export default ServicesText;