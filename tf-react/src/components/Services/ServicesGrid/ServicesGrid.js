import React, { Component } from 'react';
import ServiceCard from '../ServiceCard/ServiceCard';
import './ServicesGrid.css';

class ServicesGrid extends Component {
	render(){
		let renderServiceCards = this.props.data.map((s, index) => {
			return (
				<ServiceCard key={index} title={s.title} desc={s.description}/>
				)
		})
		return(
			<div className="service-card-grid">{renderServiceCards}</div>
			)
	}
}

export default ServicesGrid;