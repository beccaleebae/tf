import React, { Component } from 'react';
import './ServiceCard.css';

class ServiceCard extends Component {
	render(){
		return(
			<div className="service-card">
				<p className="service-title">{this.props.title}</p>
				<p className="service-desc">{this.props.desc}</p>
			</div>
			)
	}
}

export default ServiceCard;