import React, { Component } from "react";
import PageDivider from "../PageDivider/PageDivider";
import ServicesText from "./ServicesText/ServicesText";
import ServicesGrid from "./ServicesGrid/ServicesGrid";
import Loading from "../Loading/Loading";
import "./Services.css";

class Services extends Component {
	componentWillMount() {
		this.props.changeRoute();
		this.props.getPageData("services", "servicesData");
	}
	render() {
		if (this.props.responseOk === true) {
			return (
				<div className="container">
					<div className="services-page">
						<PageDivider marginLg={true} />
						{this.props.servicesData && (
							<ServicesText
								mainCopy={this.props.servicesData.mainCopy}
								subCopy={this.props.servicesData.subCopy}
							/>
						)}
						{this.props.servicesData.services && (
							<ServicesGrid data={this.props.servicesData.services} />
						)}
					</div>
				</div>
			);
		} else {
			return <Loading />;
		}
	}
}

export default Services;