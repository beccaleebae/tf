import React, { Component } from "react";
// Hi-Level Components
import Nav from "./components/Nav/Nav";
import Footer from "./components/Footer/Footer";
// Page Components
import Home from "./components/Home/Home";
import Work from "./components/Work/Work";
import SingleProject from "./components/SingleProject/SingleProject";
import Services from "./components/Services/Services";
import Team from "./components/Team/Team";
import Contact from "./components/Contact/Contact";
import Page404 from "./components/Page404/Page404";
// Assets
import Logo from "./assets/ToreadorFilmsLogo.png";
import FB from "./assets/icons/facebook.png";
import VM from "./assets/icons/vimeo.png";
import BrokenPage from "./assets/icons/404.png";
// Etc
import axios from "axios";
import { Switch, Route } from "react-router-dom";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "http://54.87.243.169/api",
      responseOk: false,
      tabs: ["work", "services", "team", "contact"],
      showNavShadow: false,
      footerLinks: [
        { icon: FB, link: "https://www.facebook.com/toreadorfilms/" },
        { icon: VM, link: "https://vimeo.com/toreadorfilms" }
      ],
      homePageData: {},
      viewCommercialWork: false,
      workPageData: {},
      projectData: {},
      servicesData: {},
      teamPageData: [],
      contactData: {},
      emailFormData: {
        firstName: "",
        lastName: "",
        email: "",
        subject: "",
        body: ""
      }
    };
    this.toggleWorkGrid = this.toggleWorkGrid.bind(this);
    this.checkIfScrollingDown = this.checkIfScrollingDown.bind(this);
    this.getPageData = this.getPageData.bind(this);
    this.changeRoute = this.changeRoute.bind(this);
  }
  getPageData(endpoint, stateName) {
    axios
      .get(`${this.state.url}/${endpoint}`)
      .then(res => {
        window.addEventListener("scroll", this.checkIfScrollingDown);
        this.setState({ [stateName]: res.data });
        setTimeout(() => {
          this.setState({ responseOk: true });
        }, 1500);
      })
      .catch(error => {
        window.location = "/404";
      });
  }
  changeRoute() {
    this.setState({ responseOk: false });
    console.log("Resetting state");
  }
  checkIfScrollingDown() {
    let el = document.querySelector(".container");

    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    if (elemTop < 115) {
      this.setState({ showNavShadow: true });
    } else {
      this.setState({ showNavShadow: false });
    }
  }
  toggleWorkGrid() {
    this.setState({
      viewCommercialWork: !this.state.viewCommercialWork
    });
  }
  onChange(e, key) {
    let emailFormData = this.state.emailFormData;
    emailFormData[key] = e.target.value;
    this.setState({ emailFormData });
  }
  onSubmit(e) {
    e.preventDefault();
    console.log(this.state.emailFormData);
    alert("Thanks! We'll get in touch with you as soon as we can.");
    window.location = '/';
  }
  render() {
    return (
      <div>
        <Nav
          logo={Logo}
          tabs={this.state.tabs}
          showNavShadow={this.state.showNavShadow}
        />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <Home
                data={this.state.homePageData}
                getPageData={this.getPageData}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
              />
            )}
          />
          <Route
            path="/work/:id"
            render={({ match }) => (
              <SingleProject
                match={match}
                data={this.state.projectData}
                getPageData={this.getPageData}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
              />
            )}
          />
          <Route
            path="/work"
            render={() => (
              <Work
                viewCommercialWork={this.state.viewCommercialWork}
                workPageData={this.state.workPageData}
                toggleWorkGrid={this.toggleWorkGrid}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
                getPageData={this.getPageData}
              />
            )}
          />
          <Route
            path="/services"
            render={() => (
              <Services
                servicesData={this.state.servicesData}
                getPageData={this.getPageData}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
              />
            )}
          />
          <Route
            path="/team"
            render={() => (
              <Team
                teamPageData={this.state.teamPageData}
                getPageData={this.getPageData}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
              />
            )}
          />
          <Route
            path="/contact"
            render={() => (
              <Contact
                contactData={this.state.contactData}
                emailFormData={this.state.emailFormData}
                onSubmit={this.onSubmit.bind(this)}
                onChange={this.onChange.bind(this)}
                getPageData={this.getPageData}
                responseOk={this.state.responseOk}
                changeRoute={this.changeRoute}
              />
            )}
          />
          <Route path="/404" render={() => <Page404 img={BrokenPage} />} />
        </Switch>
        <Footer links={this.state.footerLinks} responseOk={this.state.responseOk} />
      </div>
    );
  }
}

export default App;